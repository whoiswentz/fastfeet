module.exports = {
    env: {
        browser: true,
        es6: true
    },
    extends: ["airbnb-base"],
    globals: {
        Atomics: "readonly",
        SharedArrayBuffer: "readonly"
    },
    parser: "@typescript-eslint/parser",
    parserOptions: {
        ecmaVersion: 2018,
        sourceType: "module"
    },
    plugins: ["@typescript-eslint"],
    rules: {
        "import/extensions": "off",
        "class-methods-use-this": "off",
        "no-param-reassign": "off",
        "lines-between-class-members": "off",
        indent: ["error", 4],
        camelcase: "off",
        "no-unused-vars": ["error", { argsIgnorePattern: "next" }],
        "func-names": "off",
        "prefer-arrow-callback": "off",
        "arrow-body-style": "off",
        "max-len": "off",
        "no-useless-constructor": "off",
        "no-empty-function": "off",
        "import/no-extraneous-dependencies": ["error", {"devDependencies": true}]
    },
    overrides: [
        {
            files: ["*.ts", "*.tsx"],
            rules: {
                "@typescript-eslint/no-unused-vars": [2, { args: "none" }]
            }
        }
    ],
    settings: {
        "import/resolver": {
            node: {
                extensions: [".js", ".jsx", ".ts", ".tsx"],
                moduleDirectory: ["node_modules", "src/"]
            }
        }
    }
};
