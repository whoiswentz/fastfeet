import aws, { S3 } from 'aws-sdk';
import fs from 'fs';
import Logger from '../logger';

const logger = Logger.getInstance();

export default class FileUpload {
    private static instance: FileUpload

    private s3: S3
    private bucket: string

    private constructor() {
        if (!process.env.BUCKET_NAME) {
            throw new Error('bucket name must be configured');
        }
        this.bucket = process.env.BUCKET_NAME;
        aws.config.setPromisesDependency(null);
        aws.config.update({
            accessKeyId: process.env.ACCESSKEYID,
            secretAccessKey: process.env.SECRETACCESSKEY,
            region: process.env.REGION,
        });
        this.s3 = new aws.S3();
    }

    public async upload(file: Express.Multer.File, key: string): Promise<S3.ManagedUpload.SendData> {
        const params: S3.Types.PutObjectRequest = {
            ACL: 'public-read',
            Bucket: this.bucket,
            Body: fs.createReadStream(file.path),
            Key: `${key}/${file.originalname}`,
        };
        try {
            const data = await this.s3.upload(params).promise();
            return data;
        } catch (error) {
            logger.error({ event: 'FileUpload', error }, 'S3 Error');
            throw error;
        } finally {
            fs.unlinkSync(file.path);
        }
    }

    public static getInstance(): FileUpload {
        if (!FileUpload.instance) {
            FileUpload.instance = new FileUpload();
        }
        return FileUpload.instance;
    }
}
