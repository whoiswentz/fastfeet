export default {
    host: 'smtp.sendgrid.net',
    port: 587,
    secure: false,
    auth: {
        user: 'apikey',
        pass: process.env.SEND_GRID,
    },
    default: {
        from: 'test@test.com',
    },
};
