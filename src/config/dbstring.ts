
export class NoDatabaseParams extends Error { }

export default function connectionString(): string {
    const dbUser = process.env.DB_USER;
    const dbPassword = process.env.DB_PASSWORD;

    if (!dbUser || !dbPassword) {
        throw new NoDatabaseParams('MongoDB params must be provided');
    }

    const clusterDomain = 'knowledge-keeper-cluster0-snte3.mongodb.net';
    return `mongodb+srv://${dbUser}:${dbPassword}@${clusterDomain}/fastfeet?retryWrites=true&w=majority`;
}
