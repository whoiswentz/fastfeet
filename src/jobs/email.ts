import { parentPort } from 'worker_threads';
import nodemailer from 'nodemailer';
import Mail from 'nodemailer/lib/mailer';
import sendgridconfig from '../config/sendgrid';

async function sendMail(params: any, transporter: Mail) {
    return transporter.sendMail({
        ...sendgridconfig.default,
        ...params,
    });
}

if (parentPort) {
    parentPort.on('message', async (data: any) => {
        const { msgPort, params } = data;

        const {
            host, port, secure, auth,
        } = sendgridconfig;

        const transporter = nodemailer.createTransport({
            host, port, secure, auth,
        });

        const result = await sendMail(params, transporter);
        msgPort.postMessage(result);
    });
}
