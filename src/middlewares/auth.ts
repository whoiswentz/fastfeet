import { Request, Response, NextFunction } from 'express';
import { promisify } from 'util';
import jwt from 'jsonwebtoken';
import Logger from '../logger';

const logger = Logger.getInstance();

export interface JwtReturn {
    id: number
}

export default async function auth(req: Request, res: Response, next: NextFunction) {
    const authorazation = req.headers.authorization;
    if (!authorazation) {
        logger.warn({ event: 'AuthMiddleware' }, 'Auth token not present');
        return res.status(401).json({ error: 'Auth token not present' });
    }

    const [, token] = authorazation?.split(' ');

    try {
        const asyncVerify = promisify(jwt.verify);
        const decoded = await asyncVerify(token, 'secret') as JwtReturn;

        req.userId = decoded.id;
        return next();
    } catch (error) {
        return res.status(401).json({ error: 'Invalid token' });
    }
}
