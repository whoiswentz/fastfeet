import { Request, Response, NextFunction } from 'express';
import Logger from '../logger';
import User from '../models/User';

const logger = Logger.getInstance();

export default async function adminChecker(req: Request, res: Response, next: NextFunction) {
    const id = req.userId;

    if (!id) {
        logger.warn({ event: 'AdminChecking' }, 'Id dindt come inside the request');
        return res.status(401).json({ error: 'Unotharized' });
    }

    const user = await User.findById(id);
    if (!user?.admin) {
        logger.error({ user: user?.email }, 'Its trying to perform an admin operation');
        return res.status(401).json({ error: 'Unotharized' });
    }

    return next();
}
