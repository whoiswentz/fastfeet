import pino from 'pino';

export default class Logger {
    private static instance: pino.Logger;

    public static getInstance(): pino.Logger {
        if (!Logger.instance) {
            Logger.instance = pino({
                level: process.env.LOG_LEVEL || 'info',
                prettyPrint: {
                    levelFirst: true,
                },
            });
        }
        return Logger.instance;
    }
}
