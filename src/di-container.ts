import { Container } from '../node_modules/inversify';
import ConfigurationRepository from './repositories/ConfigurationRepository';
import UserRepository from './repositories/UserRepository';
import FileRepository from './repositories/FileRepository';
import DeliverymanRepository from './repositories/DeliverymanRepository';
import OrderRepository from './repositories/OrderRepository';
import RecipientRepository from './repositories/RecipientRepository';
import DeliveryProblemRepository from './repositories/DeliveryProblemRepository';

const DependencyInjection = new Container();

DependencyInjection.bind<ConfigurationRepository>(ConfigurationRepository).toSelf();
DependencyInjection.bind<UserRepository>(UserRepository).toSelf();
DependencyInjection.bind<FileRepository>(FileRepository).toSelf();
DependencyInjection.bind<DeliverymanRepository>(DeliverymanRepository).toSelf();
DependencyInjection.bind<OrderRepository>(OrderRepository).toSelf();
DependencyInjection.bind<RecipientRepository>(RecipientRepository).toSelf();
DependencyInjection.bind<DeliveryProblemRepository>(DeliveryProblemRepository).toSelf();

export default DependencyInjection;
