import { Request, Response } from 'express';
import { format } from 'date-fns';
import { injectable, inject } from 'inversify';
import { isMainThread, Worker, MessageChannel } from 'worker_threads';
import { resolve } from 'path';
import DeliverymanRepository from '../repositories/DeliverymanRepository';
import OrderRepository from '../repositories/OrderRepository';
import ConfigurationRepository from '../repositories/ConfigurationRepository';
import RecipientRepository from '../repositories/RecipientRepository';
import Logger from '../logger';
import FileRepository from '../repositories/FileRepository';
import FileUpload from '../utils/FileUpload';
import { OrderStatus } from '../models/Order';
import DeliveryProblemRepository from '../repositories/DeliveryProblemRepository';

const logger = Logger.getInstance();

@injectable()
class OrderController {
    constructor(
        @inject(ConfigurationRepository) private configRepo: ConfigurationRepository,
        @inject(OrderRepository) private orderRepo: OrderRepository,
        @inject(RecipientRepository) private recipientRepo: RecipientRepository,
        @inject(DeliverymanRepository) private deliverymanRepo: DeliverymanRepository,
        @inject(FileRepository) private fileRepo: FileRepository,
        @inject(DeliveryProblemRepository) private deliveryProblem: DeliveryProblemRepository,
    ) { }

    public store = async (req: Request, res: Response): Promise<Response> => {
        const configuration = await this.configRepo.findOne({ user: req.userId });
        if (!configuration?.open_days) {
            return res.status(404).json({ error: 'Please setup your open days' });
        }

        const {
            recipient, deliveryman, product,
        } = req.body;

        const existendRecipient = await this.recipientRepo.findOne({ _id: recipient });
        if (!existendRecipient) {
            return res.status(404).json({ error: 'Invalid recipient' });
        }

        const exitentDeliveryman = await this.deliverymanRepo.findOne({ _id: deliveryman });
        if (!exitentDeliveryman) {
            return res.status(404).json({ error: 'Invalid deliveryman' });
        }

        const reqDate = new Date();
        const dayOftheWeek = format(reqDate, 'EEEE').toLocaleLowerCase();
        const { opens, closes_at, opens_at } = configuration.open_days[dayOftheWeek];
        if (!opens) {
            return res.status(401).json({ error: 'We don\'t open today' });
        }

        const requestHour = format(reqDate, 'HH:mm');
        const openHours = format(new Date(opens_at), 'HH:mm');
        const closeHours = format(new Date(closes_at), 'HH:mm');

        if (requestHour <= openHours || requestHour >= closeHours) {
            return res.status(403).json({ error: 'We are closed' });
        }

        if (isMainThread) {
            const worker = new Worker(resolve(__dirname, '..', 'jobs', 'email.js'));
            const { port1, port2 } = new MessageChannel();

            worker.postMessage({
                msgPort: port2,
                params: {
                    to: exitentDeliveryman.email,
                    subject: 'You received a new order',
                    text: 'You received a new order, you can pick up in the HQ',
                },
            }, [port2]);

            port1.on('message', (message) => {
                logger.info(message);
            });
        }

        const order = await this.orderRepo.create({
            recipient,
            deliveryman,
            product,
            status: OrderStatus.TO_REMOVE,
        });
        return res.json(order);
    }

    public update = async (req: Request, res: Response): Promise<Response> => {
        const { orderId } = req.params;

        try {
            const existedOrder = await this.orderRepo.findOne({ _id: orderId });
            if (existedOrder?.cancelated_at) {
                return res.status(403).json({ error: 'This order it\'s already cancelated' });
            }

            if (existedOrder?.end_date && existedOrder.signature) {
                return res.status(403).json({ error: 'This order it\'s already has been delivered' });
            }

            const {
                recipient, deliveryman, product, start_date, cancelated_at, end_date,
            } = req.body;

            const existendRecipient = await this.recipientRepo.findOne({ _id: recipient });
            const exitentDeliveryman = await this.deliverymanRepo.findOne({ _id: deliveryman });
            if (!existendRecipient || !exitentDeliveryman) {
                return res.status(404).json({ error: 'Invalid recipient or deliveryman' });
            }

            if (cancelated_at) {
                const cancelatedOrder = await existedOrder?.updateOne({ cancelated_at });
                return res.status(200).json(cancelatedOrder);
            }

            const updatedOrder = await existedOrder?.updateOne({
                recipient, deliveryman, product, start_date, end_date,
            });
            return res.status(200).json(updatedOrder);
        } catch (error) {
            return res.status(500).json({
                error: 'Fatal Error Occurred',
            });
        }
    }

    public getOrderToDelivery = async (req: Request, res: Response): Promise<Response> => {
        const { orderId } = req.params;
        const { start_date, end_date, deliverymanId } = req.body;

        try {
            const existedOrder = await this.orderRepo.findOne({ _id: orderId });
            if (existedOrder?.cancelated_at) {
                return res.status(403).json({ error: 'This order it\'s already cancelated' });
            }

            const exitentDeliveryman = await this.deliverymanRepo.findOne({ _id: deliverymanId });
            if (!exitentDeliveryman) {
                return res.status(404).json({ error: 'Invalid recipient or deliveryman' });
            }
            if (exitentDeliveryman.deliveries >= 5) {
                return res.status(403).json({ error: 'You can\'t get more orders to delivery' });
            }

            await this.deliverymanRepo.update(
                { _id: deliverymanId },
                { $inc: { deliveries: +1 } },
            );
            const updatedOrder = await this.orderRepo.update(
                { _id: orderId },
                { start_date, end_date, status: OrderStatus.IN_TRANSIT },
            );

            return res.json(updatedOrder);
        } catch (error) {
            return res.status(500).json({
                error: 'Fatal Error Occurred',
            });
        }
    }

    public finishOrder = async (req: Request, res: Response): Promise<Response> => {
        const { orderId } = req.params;
        const { deliveryman } = req.body;

        try {
            const existedOrder = await this.orderRepo.findOne({ _id: orderId });
            if (existedOrder?.cancelated_at) {
                return res.status(403).json({ error: 'This order it\'s already cancelated' });
            }
            const exitentDeliveryman = await this.deliverymanRepo.findOne({ _id: deliveryman });
            if (!exitentDeliveryman) {
                return res.status(404).json({ error: 'Invalid recipient or deliveryman' });
            }

            if (req.file && !existedOrder?.signature) {
                const updatedFile = await FileUpload.getInstance().upload(req.file, 'customer_signatures');
                const savedFile = await this.fileRepo.create({ name: req.file.originalname, url: updatedFile.Location });
                const updatedeOrder = await existedOrder?.updateOne({
                    status: OrderStatus.DELIVERED,
                    signature: savedFile,
                });
                await this.deliverymanRepo.update(
                    { _id: deliveryman },
                    { $inc: { deliveries: -1 } },
                );
                return res.status(200).json(updatedeOrder);
            }

            return res.status(403).json({ error: 'Invalid Params' });
        } catch (error) {
            return res.status(500).json({
                error: 'Fatal Error Occurred',
            });
        }
    }

    public index = async (req: Request, res: Response): Promise<Response> => {
        try {
            const allOrders = await this.orderRepo.find();
            return res.json(allOrders);
        } catch (error) {
            return res.status(500).json({ error: 'Unknown error', stack: error });
        }
    }

    public delete = async (req: Request, res: Response): Promise<Response> => {
        const { orderId } = req.params;

        try {
            const deletedOrder = await this.orderRepo.delete({ _id: orderId });
            if (!deletedOrder) {
                return res.status(404).json({ error: 'Invalid order' });
            }

            const exitentDeliveryman = await this.deliverymanRepo.findOne({ _id: deletedOrder?.deliveryman });
            if (!exitentDeliveryman) {
                return res.status(404).json({ error: 'Invalid deliveryman' });
            }

            const problem = await this.deliveryProblem.create({
                delivery: deletedOrder,
                ...req.body,
            });


            if (isMainThread) {
                const worker = new Worker(resolve(__dirname, '..', 'jobs', 'email.js'));
                const { port1, port2 } = new MessageChannel();

                worker.postMessage({
                    msgPort: port2,
                    params: {
                        to: exitentDeliveryman.email,
                        subject: 'Problem bro',
                        text: problem.description,
                    },
                }, [port2]);

                port1.on('message', (message) => {
                    logger.info(message);
                });
            }

            return res.json(deletedOrder);
        } catch (error) {
            return res.status(500).json({ error: 'Unknown error', stack: error });
        }
    }
}

export default OrderController;
