import { Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import { injectable, inject } from 'inversify';
import Logger from '../logger';
import UserRepository from '../repositories/UserRepository';


const logger = Logger.getInstance();

@injectable()
export class SessionController {
    constructor(
        @inject(UserRepository) private userRepo: UserRepository,
    ) { }

    public store = async (req: Request, res: Response): Promise<Response> => {
        const { email, password }: { email: string, password: string } = req.body;

        try {
            const user = await this.userRepo.findOne({ email });
            if (!user) {
                logger.error({ email }, 'User not found');
                return res.status(401).json({ error: 'User not found' });
            }

            const passwordMatch = await user.checkPassword(password);
            if (!passwordMatch) {
                logger.error({ email }, 'Incorrect Password');
                return res.status(401).json({ error: 'Incorrect Password' });
            }

            const { id }: { id?: string } = user;
            return res.status(200).json({
                user: { id },
                token: jwt.sign({ id }, 'secret', { expiresIn: '7d' }),
            });
        } catch (error) {
            logger.error({ error, stack: error.stack }, 'Error Ocurred');
            return res.status(401).json({ error: error.message });
        }
    }
}

export default SessionController;
