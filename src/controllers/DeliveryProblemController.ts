import { inject, injectable } from 'inversify';
import { Request, Response } from 'express';
import * as Yup from 'yup';
import DeliveryProblemRepository from '../repositories/DeliveryProblemRepository';
import OrderRepository from '../repositories/OrderRepository';
import DeliverymanRepository from '../repositories/DeliverymanRepository';

const storeValidator = Yup.object().shape({
    deliverymanId: Yup.string().required(),
    description: Yup.string().required(),
});

@injectable()
export default class DeliveryProblemController {
    constructor(
        @inject(DeliveryProblemRepository) private deliveryProblemRepo: DeliveryProblemRepository,
        @inject(OrderRepository) private orderRepo: OrderRepository,
        @inject(DeliverymanRepository) private deliverymanRepo: DeliverymanRepository,
    ) { }

    public store = async (req: Request, res: Response): Promise<Response> => {
        const { orderId } = req.params;
        const { deliverymanId, description } = req.body;

        try {
            await storeValidator.validate(req.body);

            const order = await this.orderRepo.findOne({ _id: orderId });
            if (!order) {
                return res.status(404).json({ error: 'This order don\'t exists' });
            }

            const deliveryman = await this.deliverymanRepo.findOne({ _id: deliverymanId });
            if (!deliveryman) {
                return res.status(404).json({ error: 'This deliveryman don\'t exists' });
            }

            const deliveryProblem = await this.deliveryProblemRepo.create({
                delivery: order,
                description,
            });

            return res.json(deliveryProblem);
        } catch (error) {
            return res.status(500).json({
                error: error.name,
                message: error.message,
            });
        }
    }

    public index = async (req: Request, res: Response): Promise<Response> => {
        const { orderId } = req.params;

        try {
            const orders = await this.deliveryProblemRepo.find({ delivery: orderId });
            return res.json(orders);
        } catch (error) {
            return res.status(500);
        }
    }
}
