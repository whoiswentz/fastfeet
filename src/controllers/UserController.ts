import { Request, Response } from 'express';
import * as Yup from 'yup';
import { injectable, inject } from 'inversify';
import UserRepository from '../repositories/UserRepository';
import User from '../models/User';
import Logger from '../logger';


const logger = Logger.getInstance();

@injectable()
class UserController {
    constructor(
        @inject(UserRepository) private userRepo: UserRepository,
    ) {}

    public store = async (req: Request, res: Response): Promise<Response> => {
        try {
            const user = await this.userRepo.findOne({ email: req.body.email });
            if (user) {
                logger.error({ user: user.email }, 'User already exists');
                return res.status(400).json({ error: 'User already exists' });
            }

            const { id, name, email } = await this.userRepo.create(req.body);
            return res.json({ id, name, email });
        } catch (error) {
            logger.error({ error, stack: error.stack }, 'Error Occured');
            return res.status(500).json({ error });
        }
    }

    public async update(req: Request, res: Response): Promise<Response> {
        const schemaValidation = Yup.object().shape({
            name: Yup.string(),
            email: Yup.string().email(),
            oldPassword: Yup.string().min(6).when(
                'password',
                (password: string, field: Yup.StringSchema<string>) => {
                    return password ? field.required() : field;
                },
            ),
            password: Yup.string().min(6),
            confirmPassword: Yup.string().min(6).when(
                'password',
                (password: string, field: Yup.StringSchema<string>) => {
                    return password ? field.required().oneOf([Yup.ref('password')]) : field;
                },
            ),
        });

        const isValidSchema = await schemaValidation.isValid(req.body);
        if (!isValidSchema) {
            return res.status(400).json({ error: 'Invalid Params' });
        }

        try {
            const userToUpdate = await User.findById(req.userId);
            if (!userToUpdate) {
                return res.status(404).json({ error: 'User not found' });
            }

            const { email, oldPassword } = req.body;

            if (email && email === userToUpdate.email) {
                return res.status(401).json({ error: 'Can not update to same email' });
            }

            const match = await userToUpdate.checkPassword(oldPassword);
            if (oldPassword && !match) {
                return res.status(401).json({ error: 'Passwords does not match' });
            }

            userToUpdate.password = req.body.password;
            await userToUpdate.save();

            return res.json(userToUpdate);
        } catch (error) {
            logger.error({ error, stack: error.stack }, 'Error occured');
            return res.status(500).json({ error });
        }
    }
}

export default UserController;
