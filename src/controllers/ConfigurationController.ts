import { Request, Response } from 'express';
import { inject, injectable } from 'inversify';
import UserRepository from '../repositories/UserRepository';
import ConfigurationRepository from '../repositories/ConfigurationRepository';

@injectable()
class ConfigurationController {
    constructor(
        @inject(ConfigurationRepository) private configRepo: ConfigurationRepository,
        @inject(UserRepository) private userRepo: UserRepository,
    ) { }

    public store = async (req: Request, res: Response): Promise<Response> => {
        try {
            const configuration = await this.configRepo.findOne({ user: req.userId });
            if (!configuration) {
                const newConfiguration = await this.configRepo.create({ ...req.body, user: req.userId });
                return res.json({ newConfiguration });
            }

            const updatedConfiguration = await configuration.updateOne(req.body);
            return res.json(updatedConfiguration);
        } catch (error) {
            return res.status(500).json({ error: 'Unknown error', stack: error });
        }
    }
}

export default ConfigurationController;
