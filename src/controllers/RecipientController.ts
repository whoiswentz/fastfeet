import { Request, Response } from 'express';
import { injectable, inject } from 'inversify';
import RecipientRepository from '../repositories/RecipientRepository';
import Logger from '../logger';


const logger = Logger.getInstance();

@injectable()
class RecipientController {
    constructor(
        @inject(RecipientRepository) private recipientRepo: RecipientRepository,
    ) { }


    public store = async (req: Request, res: Response): Promise<Response> => {
        try {
            const recipient = await this.recipientRepo.create(req.body);
            return res.json(recipient);
        } catch (error) {
            logger.error({ error, stack: error.stack }, 'Error occured');
            return res.status(500).json({ error });
        }
    }
}

export default RecipientController;
