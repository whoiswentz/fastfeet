import { Request, Response } from 'express';
import * as Yup from 'yup';
import { injectable, inject } from 'inversify';
import { DeliverymanUpdateParams } from '../models/Deliveryman';
import DeliverymanRepository from '../repositories/DeliverymanRepository';
import FileUpload from '../utils/FileUpload';
import Logger from '../logger';
import FileRepository from '../repositories/FileRepository';
import OrderRepository from '../repositories/OrderRepository';

const logger = Logger.getInstance();

const creationSchemaValidation = Yup.object().shape({
    name: Yup.string().required(),
    email: Yup.string().required(),
});

@injectable()
class DeliverymanController {
    constructor(
        @inject(FileRepository) private fileRepo: FileRepository,
        @inject(DeliverymanRepository) private deliverymanRepo: DeliverymanRepository,
        @inject(OrderRepository) private orderRepo: OrderRepository,
    ) { }

    public store = async (req: Request, res: Response): Promise<Response> => {
        const isValidSchema = await creationSchemaValidation.isValid(req.body);
        if (!isValidSchema) {
            return res.status(403).json({ error: 'Invalid params' });
        }

        if (!req.file) {
            return res.status(403).json({ error: 'Deliveryman must have a avatar' });
        }

        try {
            const uploadedFile = await FileUpload.getInstance().upload(req.file, 'deliverymanAvatar');
            const savedFile = await this.fileRepo.create({ name: req.file.originalname, url: uploadedFile.Location });
            const deliveryman = await this.deliverymanRepo.create({
                ...req.body,
                avatar: savedFile.id,
                deliveries: 0,
            });
            return res.json(deliveryman);
        } catch (error) {
            logger.error({
                event: 'Deliveryman Creation',
                error,
            }, 'FatalError');
            return res.status(500).json({ error: error.message });
        }
    }

    public getAllDeliverymanOrders = async (req: Request, res: Response): Promise<Response> => {
        const { deliverymanId } = req.params;

        try {
            const allOrder = await this.orderRepo.find({
                deliveryman: deliverymanId,
                cancelated_at: undefined,
                end_date: undefined,
            });

            return res.json(allOrder);
        } catch (error) {
            return res.status(500).json({ error: error.message });
        }
    }

    public index = async (req: Request, res: Response): Promise<Response> => {
        try {
            const deliverymen = await this.deliverymanRepo.find(['avatar']);
            if (!deliverymen) {
                return res.status(403).json({ error: 'No deliverymen registered' });
            }
            return res.json(deliverymen);
        } catch (error) {
            logger.error({
                event: 'Deliveryman GetAll',
                error,
            }, 'FatalError');
            return res.status(500).json({ error: error.message });
        }
    }

    public delete = async (req: Request, res: Response): Promise<Response> => {
        const { id } = req.body;
        if (!id) {
            return res.status(403).json({ error: 'Id must be provided for deliveryman removal' });
        }

        try {
            const deliveryman = await this.deliverymanRepo.delete({ _id: id });
            if (!deliveryman) {
                return res.status(403).json({ error: 'No deliveryman with this id registered' });
            }
            return res.json(deliveryman);
        } catch (error) {
            logger.error({
                event: 'Deliveryman Deletion',
                error,
            }, 'FatalError');
            return res.status(500).json({ error: error.message });
        }
    }

    public update = async (req: Request, res: Response): Promise<Response> => {
        const { id } = req.params;
        if (!id) {
            return res.status(403).json({ error: 'Id must be present' });
        }

        const isValidSchema = await creationSchemaValidation.isValid(req.body);
        if (!isValidSchema) {
            return res.status(403).json({ error: 'Invalid params' });
        }

        try {
            const deliveryman = await this.deliverymanRepo.findOne({ _id: id });
            if (!deliveryman) {
                return res.status(404).json({ error: 'Deliveryman not found' });
            }

            const { name, email } = req.body;
            const updateParams: DeliverymanUpdateParams = { name, email };

            if (req.file) {
                const uploadedFile = await FileUpload.getInstance().upload(req.file, 'deliverymanAvatar');
                const savedFile = await this.fileRepo.create({ name: req.file.originalname, url: uploadedFile.Location });
                updateParams.avatar = savedFile.id;
            }

            await deliveryman.updateOne(updateParams);

            return res.json(deliveryman);
        } catch (error) {
            logger.error({
                event: 'Deliveryman Update',
                error,
            }, 'FatalError');
            return res.status(500).json({ error: error.message });
        }
    }
}

export default DeliverymanController;
