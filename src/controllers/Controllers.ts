import DependencyInjection from '../di-container';
import DeliverymanController from './DeliverymanController';
import ConfigurationController from './ConfigurationController';
import UserController from './UserController';
import OrderController from './OrderController';
import RecipientController from './RecipientController';
import SessionController from './SessionController';
import DeliveryProblemController from './DeliveryProblemController';

const configurationController: ConfigurationController = DependencyInjection.resolve<ConfigurationController>(ConfigurationController);
const sessionController: SessionController = DependencyInjection.resolve<SessionController>(SessionController);
const deliverymanController: DeliverymanController = DependencyInjection.resolve<DeliverymanController>(DeliverymanController);
const userController: UserController = DependencyInjection.resolve<UserController>(UserController);
const orderController: OrderController = DependencyInjection.resolve<OrderController>(OrderController);
const recipientController: RecipientController = DependencyInjection.resolve<RecipientController>(RecipientController);
const deliveryProblemController: DeliveryProblemController = DependencyInjection.resolve<DeliveryProblemController>(DeliveryProblemController);

export default {
    deliverymanController,
    configurationController,
    userController,
    orderController,
    recipientController,
    sessionController,
    deliveryProblemController,
};
