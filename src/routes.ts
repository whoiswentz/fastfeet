import {
    Router, Request, Response, NextFunction,
} from 'express';
import multer from 'multer';
import auth from './middlewares/auth';
import adminChecker from './middlewares/adminChecker';
import controllers from './controllers/Controllers';

const router: Router = Router();

const avatarUpload = multer({
    dest: 'temp/',
    limits: {
        fieldSize: 8 * 1024 * 1024,
    },
}).single(
    'avatar',
);

// Fallthrough error handler
router.use(async (error: Error, req: Request, res: Response, next: NextFunction) => {
    const errors = res.status(500).json(error);
    return errors;
});

router.post('/sessions', controllers.sessionController.store);
router.post('/users', controllers.userController.store);

router.get('/deliveryman/:deliverymanId/deliveries', controllers.deliverymanController.getAllDeliverymanOrders);
router.put('/orders/:orderId/finishOrder', avatarUpload, controllers.orderController.finishOrder);
router.put('/orders/:orderId/getOrderToDelivery', controllers.orderController.getOrderToDelivery);

router.post('/delivery/:orderId/problems', controllers.deliveryProblemController.store);
router.get('/delivery/:orderId/problems', controllers.deliveryProblemController.index);

router.use(auth);
router.put('/users', controllers.userController.update);

router.use(adminChecker);
router.post('/recipients', controllers.recipientController.store);

router.post('/deliveryman', avatarUpload, controllers.deliverymanController.store);
router.get('/deliveryman', controllers.deliverymanController.index);
router.delete('/deliveryman', controllers.deliverymanController.delete);
router.put('/deliveryman/:id', avatarUpload, controllers.deliverymanController.update);

router.post('/configuration', controllers.configurationController.store);

router.post('/orders', controllers.orderController.store);
router.get('/orders', controllers.orderController.index);
router.put('/orders/:orderId', avatarUpload, controllers.orderController.update);
router.delete('/orders/:orderId', controllers.orderController.delete);

export default router;
