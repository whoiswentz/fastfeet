import express, { Express, RequestHandler, Router } from 'express';
import * as Sentry from '@sentry/node';
import Logger from './logger';

const logger = Logger.getInstance();

export default class App {
    private server: Express
    private port: number

    constructor(port: number) {
        this.server = express();
        this.port = port;
        Sentry.init({ dsn: process.env.SENTRY_DSN });
    }

    public globalMiddlewares(middlewares: RequestHandler[]): this {
        this.server.use(Sentry.Handlers.requestHandler());
        this.server.use(...middlewares);
        logger.info({
            middlewares: middlewares.length,
        }, 'Global middlewares');
        return this;
    }

    public routes(routes: Router): App {
        this.server.use(routes);
        logger.info({
            routes: routes.stack
                .filter((route: any) => route.route)
                .map((route) => route.route.path).length,
        }, 'routes setted up');
        this.server.use(Sentry.Handlers.errorHandler());
        return this;
    }

    public listen(): void {
        this.server.listen(this.port, () => {
            logger.info(
                {
                    endpoint: `http://localhost:${this.port}`,
                    environment: process.env.NODE_ENV,
                },
                'App is running!',
            );
        });
    }
}
