import mongoose, { Document, Schema } from 'mongoose';
import File, { IFile } from './File';

export interface DeliverymanUpdateParams {
    name: string
    avatar?: IFile
    email: string
    deliveries?: number
    [key: string]: any
}

export type IDeliveryman = Document & {
    name: string
    avatar: IFile
    email: string
    deliveries: number
}

const DeliverymanSchema = new Schema({
    name: {
        type: String,
        required: true,
    },
    avatar: {
        type: mongoose.Types.ObjectId,
        ref: File,
        required: true,
    },
    deliveries: {
        type: Number,
        required: true,
    },
    email: {
        type: String,
        required: true,
    },
}, {
    timestamps: true,
});

export default mongoose.model<IDeliveryman>('deliveryman', DeliverymanSchema);
