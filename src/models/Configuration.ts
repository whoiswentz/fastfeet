import mongoose, { Document, Schema } from 'mongoose';
import User, { IUser } from './User';

export type IConfiguration = Document & {
    user: IUser,
    open_days: OpenDays,
}

export interface OpenDays {
    monday: OpenHours
    tuesday: OpenHours
    wednesday: OpenHours
    thursday: OpenHours
    friday: OpenHours
    saturday: OpenHours
    sunday: OpenHours
    [key: string]: OpenHours
}

type OpenHours = {
    opens: boolean
    opens_at: Date
    closes_at: Date
}

const ConfigurationSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: User,
    },
    open_days: {
        type: Schema.Types.Mixed,
    },
});

export default mongoose.model<IConfiguration>('configuration', ConfigurationSchema);
