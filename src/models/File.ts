import mongoose, { Document, Schema } from 'mongoose';

export interface FileDTO {
    name: string
    url: string
}

export type IFile = Document & {
    name: string
    url: string
}

const FileSchema = new Schema<File>({
    name: { type: String },
    url: { type: String, required: true },
});

export default mongoose.model<IFile>('file', FileSchema);
