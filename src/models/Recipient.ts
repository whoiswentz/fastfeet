import mongoose, { Schema, Document } from 'mongoose';

export type IRecipient = Document & {
    name: string
    street: string
    complement: string
    state: string
    city: string
    zipcode: string
}

const RecipientSchema = new Schema<IRecipient>({
    name: { type: String, required: true },
    street: { type: String, required: true },
    complement: { type: String, required: true },
    city: { type: String, required: true },
    zipcode: { type: String, required: true },
}, { timestamps: true });

export default mongoose.model<IRecipient>('Recipient', RecipientSchema);
