import mongoose, { Document, Schema } from 'mongoose';
import Recipient, { IRecipient } from './Recipient';
import Deliveryman, { IDeliveryman } from './Deliveryman';
import File, { IFile } from './File';

export enum OrderStatus {
    TO_REMOVE = 'TO_REMOVE',
    IN_TRANSIT = 'IN_TRANSIT',
    DELIVERED = 'DELIVERED',
    ERROR = 'ERROR'
}

export type IOrder = Document & {
    recipient: IRecipient
    deliveryman: IDeliveryman
    signature: IFile
    product: string
    status: OrderStatus
    cancelated_at: Date
    start_date: Date
    end_date: Date
}

const OrderSchema = new Schema({
    recipient: {
        type: mongoose.Types.ObjectId,
        ref: Recipient,
        required: true,
    },
    deliveryman: {
        type: mongoose.Types.ObjectId,
        ref: Deliveryman,
        required: true,
    },
    signature: {
        type: mongoose.Types.ObjectId,
        ref: File,
    },
    status: {
        type: OrderStatus,
    },
    product: {
        type: String,
        required: true,
    },
    cancelated_at: {
        type: Date,
    },
    start_date: {
        type: Date,
    },
    end_date: {
        type: Date,
    },
}, {
    timestamps: true,
});

export default mongoose.model<IOrder>('order', OrderSchema);
