import mongoose, { Document, Schema } from 'mongoose';
import Order, { IOrder } from './Order';

export type IDeliveryProblem = Document & {
    delivery: IOrder
    description: string
}

const DeliveryProblemSchema = new Schema({
    delivery: {
        type: Schema.Types.ObjectId,
        ref: Order,
    },
    description: {
        type: String,
        required: true,
    },
}, { timestamps: true });

export default mongoose.model<IDeliveryProblem>('delivery_problem', DeliveryProblemSchema);
