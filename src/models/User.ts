import mongoose, {
    Schema, Document, HookNextFunction,
} from 'mongoose';
import bcrypt from 'bcryptjs';

export type IUser = Document & {
    name: string
    email: string
    admin: boolean
    password: string

    checkPassword(password: string): Promise<string>
    getUpdate(): Object
}

export const UserSchema = new Schema({
    name: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    admin: { type: Boolean, required: false, default: false },
    password: { type: String },
}, { timestamps: true });

UserSchema.methods.checkPassword = async function (password: string): Promise<boolean> {
    const match = await bcrypt.compare(password, this.password);
    return match;
};

UserSchema.pre<IUser>('save', async function (next: HookNextFunction): Promise<Schema<IUser>> {
    this.password = await bcrypt.hash(this.password, 8);
    return next();
});

export default mongoose.model<IUser>('User', UserSchema);
