import 'reflect-metadata';
import { Model, Document } from 'mongoose';
import { unmanaged, injectable } from 'inversify';

export interface IRead<T> {
    find(populateParams?: string[]): Promise<T[]>;
    findOne(onditions: any, populateParams: string[]): Promise<T | undefined>;
    findOneAndUpdate(conditions: any, update: any): Promise<T | null>
}

export interface IWrite<T> {
    create(doc: T): Promise<T>;
    update(conditions: any, item: T): Promise<T>;
    delete(conditions: any): Promise<T | null>;
}

@injectable()
export abstract class BaseRepository<T extends Document> implements IWrite<T>, IRead<T> {
    public readonly schema: Model<T>

    constructor(@unmanaged() schema: Model<T>) {
        this.schema = schema;
    }

    public async create(doc: any): Promise<T> {
        const result = this.schema.create(doc);
        return result;
    }

    public async update(conditions: any, item: any): Promise<T> {
        const result = await this.schema.updateOne(conditions, item);
        return result;
    }

    public async delete(conditions: any): Promise<T | null> {
        const result = await this.schema.findOneAndDelete(conditions);
        return result;
    }

    public async find(conditions?: any, populateParams?: string[]): Promise<T[]> {
        if (populateParams) {
            const deliverymen = this.schema.find(conditions).populate(populateParams);
            return deliverymen;
        }
        const deliverymen = this.schema.find(conditions);
        return deliverymen;
    }

    public async findOneAndUpdate(conditions: any, update: any): Promise<T | null> {
        const result = await this.schema.findOneAndUpdate(conditions, update);
        return result;
    }

    public async findOne(conditions: any, populateParams?: string[]): Promise<T | undefined> {
        let result: T | null;
        if (populateParams) {
            result = await this.schema.findOne(conditions).populate(populateParams);
        }
        result = await this.schema.findOne(conditions);

        if (result) {
            return result;
        }
        return undefined;
    }
}
