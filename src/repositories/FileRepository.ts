import 'reflect-metadata';
import { injectable } from 'inversify';
import File, { IFile } from '../models/File';
import { BaseRepository } from './BaseRepository';

@injectable()
class FileRepository extends BaseRepository<IFile> {
    constructor() {
        super(File);
    }
}

export default FileRepository;
