import Recipient, { IRecipient } from '../models/Recipient';
import { BaseRepository } from './BaseRepository';

class RecipientRepository extends BaseRepository<IRecipient> {
    constructor() {
        super(Recipient);
    }
}

export default RecipientRepository;
