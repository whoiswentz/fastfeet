import { BaseRepository } from './BaseRepository';
import DeliveryProblem, { IDeliveryProblem } from '../models/DeliveryProblem';

export default class DeliveryProblemRepository extends BaseRepository<IDeliveryProblem> {
    constructor() {
        super(DeliveryProblem);
    }
}
