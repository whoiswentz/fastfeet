import 'reflect-metadata';
import { injectable } from 'inversify';
import Configuration, { IConfiguration } from '../models/Configuration';
import { BaseRepository } from './BaseRepository';

@injectable()
class ConfigurationRepository extends BaseRepository<IConfiguration> {
    constructor() {
        super(Configuration);
    }
}

export default ConfigurationRepository;
