import 'reflect-metadata';
import { injectable } from 'inversify';
import Deliveryman, { IDeliveryman } from '../models/Deliveryman';
import { BaseRepository } from './BaseRepository';

export interface DeliverymanCreateParams {
    name: string
    avatar?: string
    email: string
}

@injectable()
class DeliverymanRepository extends BaseRepository<IDeliveryman> {
    constructor() {
        super(Deliveryman);
    }
}

export default DeliverymanRepository;
