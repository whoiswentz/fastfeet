import 'reflect-metadata';
import { injectable } from 'inversify';
import User, { IUser } from '../models/User';
import { BaseRepository } from './BaseRepository';

@injectable()
class UserRepository extends BaseRepository<IUser> {
    constructor() {
        super(User);
    }
}

export default UserRepository;
