import { injectable } from 'inversify';
import { BaseRepository } from './BaseRepository';
import Order, { IOrder } from '../models/Order';

@injectable()
class OrderRepository extends BaseRepository<IOrder> {
    constructor() {
        super(Order);
    }
}

export default OrderRepository;
