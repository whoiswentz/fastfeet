import mongoose, { ConnectionOptions } from 'mongoose';
import express from 'express';
import connectionString from './config/dbstring';
import Logger from './logger';
import App from './app';
import router from './routes';
import 'dotenv/config';
import 'express-async-errors';

const logger = Logger.getInstance();

const options: ConnectionOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: true,
};

mongoose.set('debug', process.env.NODE_ENV === 'development');

const startApplication = async () => {
    try {
        const db = await mongoose.connect(connectionString(), options);
        logger.info({ state: db.STATES[db.connection.readyState] }, 'MongoDB Connected');
        new App(3000).globalMiddlewares([express.json()]).routes(router).listen();
    } catch (error) {
        logger.error({ error, stack: error.stack }, 'Some Error Occured');
    }
};

startApplication();
